Trade.tf-Enhancer
=================

Converts Trade.tf's All Listings into a pseudo Live Deals

This script provides options on [trade.tf](http://trade.tf)'s all listings page, to filter trades

More information is available in the wiki.

Track its development here: [Trello](https://trello.com/b/birdRc14/trade-tf-enhancer)